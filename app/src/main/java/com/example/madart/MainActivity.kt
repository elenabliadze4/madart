package com.example.madart

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.madart.adapter.CustomAdapter
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private val img = arrayOf(R.drawable.mada2,R.drawable.mada3,R.drawable.mada4,R.drawable.mada5,
        R.drawable.mada6,R.drawable.mada7,R.drawable.mada8,R.drawable.mada9,R.drawable.mada10,
        R.drawable.mada11,R.drawable.mada12,R.drawable.mada13,R.drawable.mada14)

    private val texts = arrayOf("შოკოლადის ტორტი ბანანით","ხილის ტორტი","ტყუპი ტორტი", "შავი პრინცი","შოკოლადის ტორტი",
    "შოკოლადის ტორტი ალუბლით","ტარტალეტკა ჟოლოს კონფიტიურით","შავი ციყვი",
        "თეთრი ციყვი","ანიუტა","იდეალი","მისქეიქი","შავი მედოკი")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNavView : BottomNavigationView = findViewById (R.id.bottomNavigationView)
        val controller = findNavController(R.id.nav_host_fragment)
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = CustomAdapter(img,texts)


        val fragmentSet = setOf<Int>(
            R.id.fragmentHome2,
            R.id.fragmentProfile2,

            )

        setupActionBarWithNavController(controller , AppBarConfiguration(fragmentSet) )
        bottomNavView.setupWithNavController(controller)



    }

}